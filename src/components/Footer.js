import logoWhite from "../assets/logo_white.png";
import linkedin from "../assets/Linkedin.png";
import facebook from "../assets/Facebook.png";
import instagram from "../assets/Instagram.png";
import phone from "../assets/footer/Phone.png";
import Whatsapp from "../assets/footer/WhatsApp.png";
import email from "../assets/footer/mail.png";

function Footer() {
  return (
    <div>
      <div className="bg-[#010206] py-10 border-y-2 px-4">
        <div className="flex flex-row">
          <img src={logoWhite} alt="" />
          <p className="text-white ml-10 my-auto text-xl">
            PT. Iot Kreasi Indonesia
          </p>
        </div>
        <div className="grid grid-cols-3 gap-3 py-10">
          <div className="text-[#F5222D] font-semibold pl-4 text-start">
            JAKARTA OFFICE
            <p className="text-white font-normal my-2">
              Rukan Crown, Blok A No.23 Jl. Green Lake City Boulevard No.10,
              RT.001/RW.010, Petir, Kec. Cipondoh, Kota Tangerang, Banten 15147
              Indonesia
            </p>
          </div>
          <div className="relative">
            <div className="text-[#F5222D] absolute left-[35%] font-semibold">
              {" "}
              CONTACT US
            </div>
            <div className="text-white absolute left-[35%] top-6">
              <p className="text-left my-2">Senin-Jumat, 8 pagi - 5 sore</p>
              <div className="flex my-2">
                <img className="mr-2 object-contain" src={phone} alt="" />
                <p>Telepon Kami</p>
              </div>
              <div className="flex my-2">
                <img className="mr-2 object-contain" src={Whatsapp} alt="" />
                <p>Whatsapp Kami</p>
              </div>
              <div className="flex my-2">
                <img className="mr-2 object-contain" src={email} alt="" />
                <p>Email Kami</p>
              </div>
            </div>
          </div>
          <div className="m-auto relative">
            <div className="text-white m-4">Want to learn more?</div>
            <div className="absolute left-4">
              <button
                type="button"
                className="rounded-sm bg-[#F5222D] p-2 text-white hover:text-white "
              >
                Contact Us
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="w-full flex flex-row relative bg-black py-10 px-4">
        <div className="text-white flex-none absolute">
          © PT IoT Kreasi Indonesia. All rights reserved.
        </div>
        <div className="flex flex-row flex-none absolute right-0">
          <p className="font-bold text-[#F5222D] px-5">TERHUBUNG</p>
          <img className="px-5" src={linkedin} alt="" />
          <img className="px-5" src={facebook} alt="" />
          <img className="px-5" src={instagram} alt="" />
        </div>
      </div>
    </div>
  );
}

export default Footer;
