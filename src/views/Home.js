// import logo from "../logo.svg";
import "../App.css";
import Navbar from "../components/Navbar";
import banner from "../assets/banner.png";
import ilustration from "../assets/ilustration.png";
// import Carousel from "../components/example";
import Footer from "../components/Footer";
import IotSolution from "../assets/logoproduct/Iotsolution.png";
import Integration from "../assets/logoproduct/integration.png";
import Platform from "../assets/logoproduct/Platform.png";
import Development from "../assets/logoproduct/development.png";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Residential from "../assets/utilities/Residential.png";
import Industrial from "../assets/utilities/Industrial.png";
import Manufacturer from "../assets/utilities/Manufacturer.png";
import Oil from "../assets/utilities/Oil.png";
import Water from "../assets/utilities/Water.png";
import Building from "../assets/utilities/Building.png";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

function Home() {
  return (
    <div className="App">
      <div>
        <Navbar />
      </div>
      {/* section 1 */}
      <section className="bg-[#010206] w-full">
        <img src={banner} className="" alt="logo" />
        <div className="w-[80%] mx-auto">
          <span className="text-7xl text-white font-bold mr-2 pb-[-50px]">
            We Are End to End{" "}
            <span className="text-[#F5222D] text-7xl font-bold ">
              IoT Solution
            </span>
          </span>

          <p className="text-white text-center mt-6 text-2xl">
            Molestias quam et eaque dolorem reiciendis. Eum ut atque nesciunt
            facilis aperiam officia voluptate. Excepturi tenetur qui{" "}
          </p>
        </div>
      </section>
      {/* section 2 */}
      <section className=" w-full min-h-[100%] bg-[#010206] pb-6">
        <div className="w-[80%] mx-auto grid grid-cols-2">
          <div className="m-auto w-[90%]">
            <div className="text-[#F5222D] w-full text-left text-4xl font-bold">
              Why Choose Us
            </div>
            <p className="text-white text-md text-justify mt-2">
              Ducimus unde minima voluptatem voluptatem asperiores omnis.
              Deleniti omnis est sint. Voluptatem provident sequi rem expedita
              voluptatem optio labore rerum. Mollitia unde sit sunt quibusdam.
              Quis atque voluptatem et sed sed quaerat quis Ducimus unde minima
              voluptatem voluptatem asperiores omnis. Deleniti omnis est sint.
              Voluptatem provident sequi rem expedita voluptatem optio labore
              rerum. Mollitia unde sit sunt quibusdam. Quis atque voluptatem et
              sed sed quaerat quis
            </p>
          </div>
          <div className="m-auto">
            <img className="w-84" src={ilustration} alt="" />
          </div>
        </div>
      </section>
      {/* section 3 */}
      <section className="grid grid-cols-3 bg-[#010206] py-10 px-16">
        <div className="my-14">
          <div className="text-justify mx-auto">
            <p className="mb-6 text-[#EF3C37]  text-4xl font-bold">
              Utilities Monitoring
            </p>
            <p className="my-6 text-white">Utilities Monitoring </p>
            <p className="my-6 text-white">Streetlight Monitoring</p>
            <p className="my-6 text-white">Predicitive Maintenance</p>
            <p className="my-6 text-white">Environment monitoring</p>
          </div>
        </div>
        <div className="col-span-2 bg-[#010206]">
          <div className="text-white font-bold text-xl text-left pl-2 my-4">
            Utilities Monitoring
          </div>
          <Carousel responsive={responsive}>
            <div className="border-2 text-white h-60 w-52 mx-2 relative">
              Residential
              <img
                className="h-28 mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Residential}
                alt=""
              />
            </div>
            <div className="border-2 text-white h-60 w-52 mx-2 relative">
              Building
              <img
                className="h-28 mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Building}
                alt=""
              />
            </div>
            <div className="border-2 text-white h-60 w-52 mx-2 relative">
              Water Distribution
              <img
                className="h-28 mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Water}
                alt=""
              />
            </div>
            <div className="border-2 text-white h-60 w-52 mx-2 relative">
              Industrial Estate
              <img
                className="h-28 mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Industrial}
                alt=""
              />
            </div>
            <div className="border-2 text-white h-60 w-52 mx-2 relative">
              Manufacturer
              <img
                className="h-28 mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Manufacturer}
                alt=""
              />
            </div>
            <div className="border-2 text-white h-60 w-52 mx-2 relative">
              Oil & Gas{" "}
              <img
                className="h-28 mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Oil}
                alt=""
              />
            </div>
          </Carousel>
          ;
        </div>
      </section>
      {/* section 4 */}
      <section className="bg-[#010206] h-auto py-10 px-16">
        <div className="flex flex-col">
          {/* title 1 */}
          <div className="flex flex-col mb-12">
            <div className="text-[#F5222D] w-full text-left text-4xl font-bold">
              Our Service
            </div>
            <div className="flex flex-row my-4">
              <div className="basis-3/4 text-white text-left">
                Lorem ipsum dolor sit amet, vince adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur.
              </div>
              <div className="basis-1/4"></div>
            </div>
          </div>
          {/* content */}
          <div className="grid grid-cols-4 gap-4">
            <div className="border-2 border-[#F5222D] rounded-tr-2xl rounded-bl-2xl rounded-br-2xl mt-14">
              <div className="flex flex-col p-6 m-auto">
                <div className="">
                  <img className="object-contain" src={IotSolution} alt="" />
                </div>
                <div className="text-white text-left my-4 text-2xl font-bold">
                  IoT Solution
                </div>
                <p className="text-white text-justify">
                  Lorem ipsum dolor sit amet, vince adipiscing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                  enim ad minim veniam, quis nostrud exercitation{" "}
                </p>
              </div>
            </div>
            <div className="border-2 border-[#F5222D] rounded-tr-2xl rounded-bl-2xl rounded-br-2xl mb-14">
              <div className="flex flex-col p-6 m-auto">
                <div className="">
                  <img className="object-contain" src={Development} alt="" />
                </div>
                <div className="text-white text-left my-4 text-2xl font-bold">
                  Software development
                </div>
                <p className="text-white text-justify">
                  Lorem ipsum dolor sit amet, vince adipiscing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                  enim ad minim veniam, quis nostrud exercitation{" "}
                </p>
              </div>
            </div>
            <div className="border-2 border-[#F5222D] rounded-tr-2xl rounded-bl-2xl rounded-br-2xl mt-14">
              <div className="flex flex-col p-6 m-auto">
                <div className="">
                  <img className="object-contain" src={Platform} alt="" />
                </div>
                <div className="text-white text-left my-4 text-2xl font-bold">
                  IoT Platform
                </div>
                <p className="text-white text-justify">
                  Lorem ipsum dolor sit amet, vince adipiscing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                  enim ad minim veniam, quis nostrud exercitation{" "}
                </p>
              </div>
            </div>
            <div className="border-2 border-[#F5222D] rounded-tr-2xl rounded-bl-2xl rounded-br-2xl mb-14">
              <div className="flex flex-col p-6 m-auto">
                <div className="">
                  <img className="object-contain" src={Integration} alt="" />
                </div>
                <div className="text-white text-left my-4 text-2xl font-bold">
                  System Integrator
                </div>
                <p className="text-white text-justify">
                  Lorem ipsum dolor sit amet, vince adipiscing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                  enim ad minim veniam, quis nostrud exercitation{" "}
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* section 5 */}
      <section className="bg-[#010206] h-auto py-10 px-16">
        <div className="text-4xl text-[#F5222D] py-6 font-bold">
          Our Product
        </div>
        <div className="grid grid-cols-3 gap-3 pt-6">
          <div className="">
            <div className="text-white font-semibold text-2xl">IoT Sensor</div>
            <p className="font-thin text-sm text-left text-white">
              Ducimus unde minima voluptatem voluptatem asperiores omnis
            </p>
          </div>
          <div className="">
            <div className="text-white font-semibold text-2xl">Smart Meter</div>
            <p className="font-thin text-sm text-left text-white">
              Ducimus unde minima voluptatem voluptatem asperiores omnis
            </p>
          </div>
          <div className="">
            <div className="text-white font-semibold text-2xl">
              Lora Gateway
            </div>
            <p className="font-thin text-sm text-left text-white">
              Ducimus unde minima voluptatem voluptatem asperiores omnis
            </p>
          </div>
        </div>
      </section>
      <Footer />
    </div>
  );
}

export default Home;
