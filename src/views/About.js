import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import Arrow from "../assets/arrow.png";
import Carousel from "react-multi-carousel";
import Company1 from "../assets/clients/Company1.png";
import Company2 from "../assets/clients/Company2.png";
import Company3 from "../assets/clients/Company3.png";
import Company4 from "../assets/clients/Company4.png";
import Company5 from "../assets/clients/Company5.png";
import Commas from "../assets/commas.png";
import example from "../assets/arrow.png";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 5,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 5,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

const responsive1 = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 3,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 3,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

function About() {
  return (
    <div className="App">
      <div>
        <Navbar />
      </div>
      <div className="bg-[#010206] px-16">
        <section className="py-8 mx-24">
          <h1 className="text-white font-bold text-4xl my-4">
            About <span className="text-[#F5222D]">IoT Kreasi Indonesia</span>{" "}
          </h1>
          <p className="text-white">
            Molestias quam et eaque dolorem reiciendis. Eum ut atque nesciunt
            facilis aperiam officia voluptate. Excepturi tenetur qui Molestias
            quam et eaque dolorem reiciendis. Eum ut atque nesciunt facilis
            aperiam officia voluptate. Excepturi tenetur qui{" "}
          </p>
        </section>
        <section className="py-8 flex flex-row w-[100%]">
          <div className=" w-[35%] ">
            <p className="text-[#F5222D] text-4xl font-bold text-left w-[70%]">
              We Believe in Lorem Ipsum Dolor Afisting
            </p>
          </div>
          <div className=" w-[65%] text-white text-justify">
            A magnam sit. Error repudiandae eum dignissimos iure nobis ea. Quas
            quo repellat sit atque qui sint ea quibusdam. Repudiandae sequi
            dolores exercitationem sunt dolore placeat qui culpa. Corporis quasi
            et. Eum voluptatem aut et quo reiciendis rem neque tempore nesciunt.
            Explicabo omnis consequatur ea sit molestiae ut dicta ut.
            Praesentium enim quia laborum illum voluptas ut impedit qui. Eum
            iure assumenda natus et omnis sed laboriosam error. Autem qui
            necessitatibus animi fugiat dolor. Rerum asperiores aut consequatur
            quia qui ut illo. Sit ut in eaque quam iure iure sit alias.
          </div>
        </section>
        <section className="py-8 mx-auto flex flex-row w-[100%]">
          <div className=" w-[35%] text-white">
            <img src={Arrow} alt="" />
          </div>
          <div className=" w-[65%] text-white flex flex-col my-auto">
            <div className="text-[#F5222D] text-3xl font-bold text-left mb-6">
              Our Vision
            </div>{" "}
            <div className="text-white text-justify">
              A magnam sit. Error repudiandae eum dignissimos iure nobis ea.
              Quas quo repellat sit atque qui sint ea quibusdam. Repudiandae
              sequi dolores exercitationem sunt dolore placeat qui culpa.
              Corporis quasi et. Eum voluptatem aut et quo reiciendis rem neque
              tempore nesciunt. Explicabo omnis consequatur ea sit molestiae ut
              dicta ut. Praesentium enim quia laborum illum voluptas ut impedit
              qui. Eum iure assumenda natus et omnis sed laboriosam error. Autem
              qui necessitatibus animi fugiat dolor. Rerum asperiores aut
              consequatur quia qui ut illo. Sit ut in eaque quam iure iure sit
              alias.
            </div>
          </div>
        </section>
        <section className="py-8 mx-24">
          <h1 className="text-[#F5222D] font-bold text-4xl my-4">
            Our Happy Clients
          </h1>
          <p className="text-white px-16">
            Ducimus unde minima voluptatem voluptatem asperiores omnis. Deleniti
            omnis est sint. Voluptatem provident sequi rem expedita voluptatem.
          </p>
          <Carousel responsive={responsive}>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company1}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company2}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company3}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company4}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company5}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company1}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company5}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company1}
                alt=""
              />
            </div>
          </Carousel>
        </section>
        <section className="py-8 mx-24">
          <h1 className="text-[#F5222D] font-bold text-4xl my-4">
            What our client says
          </h1>
          <p className="text-white px-16 my-2">
            Ducimus unde minima voluptatem voluptatem asperiores omnis. Deleniti
            omnis est sint. Voluptatem provident sequi rem expedita voluptatem.
          </p>
          <Carousel responsive={responsive1}>
            <div className="flex flex-col bg-[#f9fbff0d] rounded-sm mx-2">
              <div className="mt-4">
                <img className="mx-auto" src={Commas} alt="" />
              </div>
              <div className="text-white text-center my-6">
                When our designs need an expert opinion or approval, I know I
                can rely on your agency Thank you for all your help-I will be
                recommending you to everyone
              </div>
              <div className="flex flex-row ml-4">
                <div>
                  <img
                    className="border-white border-2 h-11 w-11 rounded-full mr-4"
                    src={example}
                    alt=""
                  />
                </div>
                <div className="flex flex-col mb-4">
                  <p className="text-white text-left">Alen Max</p>{" "}
                  <p className="text-white text-left">PERUMAHAN INDAH</p>{" "}
                </div>
              </div>
            </div>
            <div className="flex flex-col bg-[#f9fbff0d] rounded-sm mx-2">
              <div className="mt-4">
                <img className="mx-auto" src={Commas} alt="" />
              </div>
              <div className="text-white text-center my-6">
                When our designs need an expert opinion or approval, I know I
                can rely on your agency Thank you for all your help-I will be
                recommending you to everyone
              </div>
              <div className="flex flex-row ml-4">
                <div>
                  <img
                    className="border-white border-2 h-11 w-11 rounded-full mr-4"
                    src={example}
                    alt=""
                  />
                </div>
                <div className="flex flex-col mb-4">
                  <p className="text-white text-left">Alen Max</p>{" "}
                  <p className="text-white text-left">PERUMAHAN INDAH</p>{" "}
                </div>
              </div>
            </div>
            <div className="flex flex-col bg-[#f9fbff0d] rounded-sm mx-2">
              <div className="mt-4">
                <img className="mx-auto" src={Commas} alt="" />
              </div>
              <div className="text-white text-center my-6">
                When our designs need an expert opinion or approval, I know I
                can rely on your agency Thank you for all your help-I will be
                recommending you to everyone
              </div>
              <div className="flex flex-row ml-4">
                <div>
                  <img
                    className="border-white border-2 h-11 w-11 rounded-full mr-4"
                    src={example}
                    alt=""
                  />
                </div>
                <div className="flex flex-col mb-4">
                  <p className="text-white text-left">Alen Max</p>{" "}
                  <p className="text-white text-left">PERUMAHAN INDAH</p>{" "}
                </div>
              </div>
            </div>
            <div className="flex flex-col bg-[#f9fbff0d] rounded-sm mx-2">
              <div className="mt-4">
                <img className="mx-auto" src={Commas} alt="" />
              </div>
              <div className="text-white text-center my-6">
                When our designs need an expert opinion or approval, I know I
                can rely on your agency Thank you for all your help-I will be
                recommending you to everyone
              </div>
              <div className="flex flex-row ml-4">
                <div>
                  <img
                    className="border-white border-2 h-11 w-11 rounded-full mr-4"
                    src={example}
                    alt=""
                  />
                </div>
                <div className="flex flex-col mb-4">
                  <p className="text-white text-left">Alen Max</p>{" "}
                  <p className="text-white text-left">PERUMAHAN INDAH</p>{" "}
                </div>
              </div>
            </div>
            <div className="flex flex-col bg-[#f9fbff0d] rounded-sm mx-2">
              <div className="mt-4">
                <img className="mx-auto" src={Commas} alt="" />
              </div>
              <div className="text-white text-center my-6">
                When our designs need an expert opinion or approval, I know I
                can rely on your agency Thank you for all your help-I will be
                recommending you to everyone
              </div>
              <div className="flex flex-row ml-4">
                <div>
                  <img
                    className="border-white border-2 h-11 w-11 rounded-full mr-4"
                    src={example}
                    alt=""
                  />
                </div>
                <div className="flex flex-col mb-4">
                  <p className="text-white text-left">Alen Max</p>{" "}
                  <p className="text-white text-left">PERUMAHAN INDAH</p>{" "}
                </div>
              </div>
            </div>
            <div className="flex flex-col bg-[#f9fbff0d] rounded-sm mx-2">
              <div className="mt-4">
                <img className="mx-auto" src={Commas} alt="" />
              </div>
              <div className="text-white text-center my-6">
                When our designs need an expert opinion or approval, I know I
                can rely on your agency Thank you for all your help-I will be
                recommending you to everyone
              </div>
              <div className="flex flex-row ml-4">
                <div>
                  <img
                    className="border-white border-2 h-11 w-11 rounded-full mr-4"
                    src={example}
                    alt=""
                  />
                </div>
                <div className="flex flex-col mb-4">
                  <p className="text-white text-left">Alen Max</p>{" "}
                  <p className="text-white text-left">PERUMAHAN INDAH</p>{" "}
                </div>
              </div>
            </div>
          </Carousel>
        </section>
        <section className="py-8 mx-24">
          <h1 className="text-[#F5222D] font-bold text-4xl my-4">
            Our Partners
          </h1>
          <p className="text-white px-16">
            Ducimus unde minima voluptatem voluptatem asperiores omnis. Deleniti
            omnis est sint. Voluptatem provident sequi rem expedita voluptatem.
          </p>
          <Carousel responsive={responsive}>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company1}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company2}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company3}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company4}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company5}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company1}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company5}
                alt=""
              />
            </div>
            <div className="text-white h-20 w-30 mx-2 relative">
              <img
                className="mx-auto absolute bottom-0 m-auto left-0 right-0"
                src={Company1}
                alt=""
              />
            </div>
          </Carousel>
        </section>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default About;
