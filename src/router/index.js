import React from "react";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import Home from "../views/Home";
import About from "../views/About";

const router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/">
          <Route index element={<Home />} />
        </Route>
        <Route path="/about">
          <Route index element={<About />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default router;
